# Spoke™ IFRAME Content

This library enables a page to be used seamlessly in an IFRAME in Spoke™'s dashboard.  While any HTTPS page can be referenced in Spoke, pages that include this library will cause Spoke to resize to fit the page's content.  This library also allows receiving of the Spoke user's information.  This library uses Cross-Domain-safe PostMessage calls to avoid security problems.

## Getting started

###HTTPS

Because Spoke uses HTTPS for all pages, the page to be displayed in the IFRAME needs be available via HTTPS as well.

###Files

The only files needed are the spoke-iframe-content.min.js (minified, 9kb) or spoke-iframe-content.js (unminified, 21kb) files.  These are in the dist/ directory or can be downloaded via Bower:

```
bower install git@bitbucket.org:unboxedtech/spoke-iframe-content.git
```

###Reference

Add a reference to the library in each page that will be used in the IFRAME.

```
<script src='bower_components/spoke-iframe-content/dist/spoke-iframe-content.min.js'> </script>
```

##Usage

###Resizing
By simply adding the library, Spoke will communicate with the page to detect the page's size.  Spoke will resize the dashboard to accommodate the page without a scrollbar.

###Getting User Information
Spoke will provide information about the current user when the page is initialized.  This occurs asynchronously via a callback on the globally-created SpokeIFrameContent object, but is called within milliseconds of the page being loaded.  To get this information, add a script like the following:

    SpokeIFrameContent.init(function(user){
        console.log(user);
    });


The "user" object passed into the init function has the following information:


    {
        UserID,
        FirstName,
        LastName,
        NickName,
        EmployeeID,
        LoginID,
        JobCode: {
            Code,
            Name
        },
        OrgEntity: {
            Name,
            ID
        },
        Roles: {
            UserAdmin (true/false),
            ResourceAdmin (true/false),
            CourseAdmin (true/false),
            NewsAdmin (true/false),
            PowerAdmin (true/false),
            LimitedReporting (true/false),
            FullMontyReporting (true/false)
        },
        Teams (array): [{
            TeamID,
            TeamName
        }]
    }

###Links on Pages
When using a page in a Spoke IFRAME, you may want to have links that cause the parent to navigate.  To make this happen, add ```location="top"``` to all <a> tags.  This will cause the full browser window to navigate to the link instead of just the content in the IFRAME.

## License
Copyright © 2015 [Unboxed Technology](http://www.unboxedtechnology.com). Licensed under the [MIT License](LICENSE).

Includes [iframe-resizer](https://github.com/davidjbradshaw/iframe-resizer) Copyright © 2013-15 [David J. Bradshaw](https://github.com/davidjbradshaw).