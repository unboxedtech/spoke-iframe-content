/*
   File: spoke-iframe-content-ext.js
   Desc: Library for pages to work cooperatively with Spoke when
         used in an IFRAME.
   Uses: This file is expected to be bundled with iframeSizer.contentWindow.js from David J. Bradshaw
  */
  
// End iFrameSizer code
var SpokeIFrameContent = (function(){ 
	
	var _initCallback = null;
	function listen(event){
		if ( (''+ event.data).substr(0, 11) === '[SpokeUser]') {
		   if (_initCallback){
			   var obj = event.data.replace('[SpokeUser]','');
			   _initCallback(JSON.parse(obj));
		   }
		} 
	};
	if ('addEventListener' in window){
		window.addEventListener('message',listen, false);
	} else if ('attachEvent' in window){ //IE
		window.attachEvent('onmessage',listen);
	}
	
	return {
		init: function(callback){
			_initCallback = callback;
		}
	}
})();
